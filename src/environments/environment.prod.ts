export const environment = {
  production: true,
  trainerApiURL: 'https://lb-noroff-assignment-api.herokuapp.com/trainers',
  apiKEY: 'blkjuwxkrwqiwnzqlcnrnyghjeczasra',
  pokemonApiURL: 'https://pokeapi.co/api/v2/pokemon',
  pokemonImageApiURL:
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon',
};
