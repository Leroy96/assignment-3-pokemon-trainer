import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private userService: UserService,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  public onRegisterSubmit(form: NgForm) {
    const { username } = form.value;
    if (username.trim() === '') {
      return;
    }

    this.loginService.checkUser(username).subscribe({
      next: (response) => {
        // User does not exist
        if (response === undefined) {
          this.loginService.register(username).subscribe({
            next: (user) => {
              this.userService.user = user;
              this.router.navigateByUrl('trainer');
            },
            error: () => {},
            complete: () => {},
          });
        }
      },
      error: () => {},
      complete: () => {},
    });
  }
}
