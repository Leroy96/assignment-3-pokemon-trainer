import { Component } from '@angular/core';
import { IUser } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  get user(): IUser | undefined {
    return this.userService.user;
  }

  constructor(private userService: UserService) {}

  Logout() {
    this.userService.user = undefined;
    sessionStorage.removeItem('user');
  }
}
