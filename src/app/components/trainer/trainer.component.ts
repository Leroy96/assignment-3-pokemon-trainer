import { TrainerService } from 'src/app/services/trainer.service';
import { UserService } from 'src/app/services/user.service';
import { Component, Input, OnChanges } from '@angular/core';
import { IPokemon } from 'src/app/models/pokemon.model';
import { IUser } from 'src/app/models/user.model';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss'],
})
export class TrainerComponent implements OnChanges {
  @Input() user: IUser | undefined;

  @Input() trainerPokemons: IPokemon[] = [];
  newArray: any = [];

  constructor(
    private userService: UserService,
    private trainerService: TrainerService
  ) {}

  ngOnChanges() {
    this.trainerPokemons.sort(function (a, b) {
      return a.id - b.id;
    });
  }

  public removePokemon(pokemon: IPokemon) {
    console.log('clicked');

    if (!this.userService.user) {
      return;
    }
    // if (this.user === undefined) {
    //   return;
    // }
    // if (this.user.pokemon === undefined) {
    //   this.user.pokemon = [];
    // }
    this.trainerService
      .removePokemon(
        this.userService.user?.id,
        this.userService.user.pokemon,
        pokemon
      )
      .subscribe({
        next: (user: IUser) => {
          console.log(user);
          this.userService.user = user;
          sessionStorage.setItem('user', JSON.stringify(this.userService.user));
          alert('Removed pokemon');
        },
        error: () => {},
        complete: () => {},
      });
  }
}
