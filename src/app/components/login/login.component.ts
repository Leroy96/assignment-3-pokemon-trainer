import { UserService } from './../../services/user.service';
import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IUser } from '../../models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public loading: boolean = false;
  public error: string = '';

  constructor(
    private loginService: LoginService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  public onStartSubmit(form: NgForm) {
    this.loading = true;
    console.log(form.value);
    const { username } = form.value;

    this.loginService.login(username).subscribe({
      next: (user: IUser | undefined) => {
        if (user === undefined) {
          // with error checking on login.service.ts this statement is not needed
          this.error = 'No such user';
        } else {
          this.userService.user = user;
          this.router.navigateByUrl('trainer');
        }
      },
      error: () => {},
      complete: () => {
        this.loading = false;
      },
    });
  }
}
