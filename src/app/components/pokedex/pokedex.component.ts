import { IUser } from 'src/app/models/user.model';
import { PokedexService } from './../../services/pokedex.service';
import { UserService } from 'src/app/services/user.service';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { IPokemon } from './../../models/pokemon.model';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss'],
})
// export class PokedexComponent implements OnInit, OnChanges {
export class PokedexComponent {
  @Input() pokemons: IPokemon[] = [];

  allPokemons: any = [];

  constructor(
    private userService: UserService,
    private pokedexService: PokedexService
  ) {}

  // ngOnChanges() {
  //   this.pokemons.map((pokemon) => {
  //     this.allPokemons.push(pokemon.name);
  //     // console.log(pokemon.name);
  //     console.log(this.allPokemons);
  //     // if ()
  //   });
  // }

  public getSinglePokemon(pokemon: any) {
    return pokemon;
  }

  public onCatch(pokemon: IPokemon) {
    if (!this.userService.user) {
      return;
    }
    this.pokedexService
      .addPokemonToTrainer(this.userService.user?.id, [
        ...this.userService.user.pokemon,
        pokemon,
      ])
      .subscribe({
        next: (user: IUser) => {
          this.userService.user = user;
        },
        error: () => {},
        complete: () => {},
      });
  }

  public onShowInfo() {
    console.log('Show info');
  }
}
