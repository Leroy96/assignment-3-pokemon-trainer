export interface IPokemon {
  name: string;
  url: string;
  id: number;
  image?: string;
  sprites?: {
    front_default: string;
  };
}

export interface IPokemonResponse {
  count: number;
  next: string;
  previous: string;
  results: IPokemon[];
}
