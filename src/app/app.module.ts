import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { PokedexPage } from './pages/pokedex/pokedex.page';
import { HomePage } from './pages/home/home.page';
import { TrainerComponent } from './components/trainer/trainer.component';
import { TrainerPage } from './pages/trainer/trainer.page';
import { LogoComponent } from './components/logo/logo.component';
import { NavbarComponent } from './components/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePage,
    RegisterComponent,
    LoginComponent,
    PokedexComponent,
    PokedexPage,
    TrainerComponent,
    TrainerPage,
    LogoComponent,
    NavbarComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
