import { PokedexService } from './../../services/pokedex.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPokemon, IPokemonResponse } from './../../models/pokemon.model';
import { environment } from 'src/environments/environment';

const { pokemonImageApiURL } = environment;

@Component({
  selector: 'app-pokedex-page',
  templateUrl: './pokedex.page.html',
  styleUrls: ['./pokedex.page.scss'],
})
export class PokedexPage implements OnInit {
  public loading: boolean = false;
  pokemons: IPokemon[] = [];
  private nextUrl: string = '';
  private previousUrl: string = '';

  private getImageUrl(url: string): string {
    const id = Number(url.split('/').filter(Boolean).pop());
    return `${pokemonImageApiURL}/${id}.png`;
  }

  private getId(url: string): number {
    return Number(url.split('/').filter(Boolean).pop());
  }

  constructor(
    private http: HttpClient,
    private pokedexService: PokedexService
  ) {}

  ngOnInit(): void {
    this.fetchPokemon();
  }

  public fetchPokemon(): void {
    this.pokedexService.pokedex().subscribe({
      next: (response: IPokemonResponse) => {
        console.log(response.results);
        this.pokemons = response.results.map((pokemon) => {
          console.log(pokemon);
          return {
            ...pokemon,
            image: this.getImageUrl(pokemon.url),
            id: this.getId(pokemon.url),
          };
        });
        return (
          (this.nextUrl = response.next), (this.previousUrl = response.previous)
        );
      },
      error: () => {},
      complete: () => {},
    });
  }

  public onNext() {
    this.pokedexService.nextPage(this.nextUrl).subscribe({
      next: (response: IPokemonResponse) => {
        console.log(response);
        this.pokemons = response.results.map((pokemon) => {
          return {
            ...pokemon,
            image: this.getImageUrl(pokemon.url),
            id: this.getId(pokemon.url),
          };
        });
        return (
          (this.nextUrl = response.next), (this.previousUrl = response.previous)
        );
      },
      error: () => {},
      complete: () => {},
    });
  }

  public onPrevious() {
    this.pokedexService.previousPage(this.previousUrl).subscribe({
      next: (response: IPokemonResponse) => {
        console.log(response);
        this.pokemons = response.results.map((pokemon) => {
          return {
            ...pokemon,
            image: this.getImageUrl(pokemon.url),
            id: this.getId(pokemon.url),
          };
        });
        return (
          (this.nextUrl = response.next), (this.previousUrl = response.previous)
        );
      },
      error: () => {},
      complete: () => {},
    });
  }
}
