import { IPokemon } from './../../models/pokemon.model';
import { PokedexService } from './../../services/pokedex.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/models/user.model';
import { TrainerService } from 'src/app/services/trainer.service';
@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.scss'],
})
export class TrainerPage implements OnInit {
  public trainerPokemons: IPokemon[] = [];
  private responseArray: any = [];

  get user(): IUser | undefined {
    return this.userService.user;
  }

  constructor(
    private userService: UserService,
    private pokedexService: PokedexService,
    private trainerService: TrainerService
  ) {}

  ngOnInit(): void {
    if (this.user?.pokemon === undefined) {
      return;
    } else {
      for (let pokemon of this.user?.pokemon) {
        this.pokedexService.pokedex(pokemon).subscribe({
          next: (response: IPokemon) => {
            this.responseArray.push(response);
            this.trainerPokemons = this.responseArray.map((element: any) => {
              return {
                ...element,
                image: element.sprites.front_default,
              };
            });
          },
          error: () => {},
          complete: () => {
            this.trainerService.trainerPokemons = this.responseArray;
          },
        });
      }
    }
  }
}
