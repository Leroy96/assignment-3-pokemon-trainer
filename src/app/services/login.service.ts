import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUser } from '../models/user.model';
import { find, map, Observable } from 'rxjs';

const { apiKEY, trainerApiURL } = environment;

@Injectable({
  providedIn: 'root', // Singleton instance
})
export class LoginService {
  constructor(private http: HttpClient) {}

  public login(username: string): Observable<IUser | undefined> {
    return this.http
      .get<IUser[]>(trainerApiURL + '?username=' + username)
      .pipe(map((response: IUser[]) => response.pop()));
    // Map to error (better use case)
  }

  public register(username: string): Observable<IUser | undefined> {
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKEY,
    });

    return this.http.post<IUser>(
      trainerApiURL,
      {
        username: username,
        pokemon: [],
      },
      { headers }
    );
  }

  public checkUser(username: string): Observable<IUser | undefined> {
    return this.http
      .get<IUser>(trainerApiURL + '?username=' + username)
      .pipe(find((element) => element.username === username));
  }
}
