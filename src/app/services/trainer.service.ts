import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { IPokemon } from 'src/app/models/pokemon.model';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { IUser } from '../models/user.model';

const { apiKEY, trainerApiURL } = environment;

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  public _trainerPokemons?: IPokemon[];

  get trainerPokemons(): IPokemon[] | undefined {
    return this._trainerPokemons;
  }

  set trainerPokemons(trainerPokemons: IPokemon[] | undefined) {
    if (trainerPokemons === undefined) {
      throw new Error('The user is undefined');
    }
    sessionStorage.setItem('t-pokemons', JSON.stringify(trainerPokemons));
    this._trainerPokemons = trainerPokemons;
  }

  constructor(private http: HttpClient, private userService: UserService) {
    const storedPokemons = sessionStorage.getItem('t-pokemons');
    if (storedPokemons) {
      this._trainerPokemons = JSON.parse(storedPokemons);
    }
  }

  ngOnInit() {
    console.log(this._trainerPokemons);
  }

  public removePokemon(
    userId: number,
    trainerPokemons: IPokemon[],
    pokemon: IPokemon
  ): Observable<IUser> {
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKEY,
    });

    return this.http.patch<IUser>(
      trainerApiURL + '/' + userId,
      {
        pokemon: [...this.remove(trainerPokemons, pokemon)],
      },
      { headers }
    );
  }

  public remove(trainerPokemons: IPokemon[], pokemon: any): IPokemon[] {
    console.log('removing');
    console.log(trainerPokemons, pokemon.name);

    for (let i = 0; i < trainerPokemons.length; i++) {
      if (trainerPokemons[i] === pokemon.name) {
        console.log('removed: ' + trainerPokemons[i]);
        trainerPokemons.splice(i, 1);
        return trainerPokemons;
      }
    }
    return trainerPokemons;
  }
}
