import { IUser } from '../models/user.model';
import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public _user?: IUser;
  @Output() public loggedIn: EventEmitter<boolean> = new EventEmitter();

  get user(): IUser | undefined {
    return this._user;
  }

  set user(user: IUser | undefined) {
    // Example
    if (!user) {
      throw new Error('The user is undefined');
    }
    sessionStorage.setItem('user', JSON.stringify(user));
    this.loggedIn.emit(true);
    this._user = user;
  }

  constructor() {
    const storedUser = sessionStorage.getItem('user');
    if (storedUser) {
      this._user = JSON.parse(storedUser);
    }
  }
}
