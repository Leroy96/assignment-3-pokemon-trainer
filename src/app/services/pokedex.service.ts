import { IUser } from 'src/app/models/user.model';
import { Observable } from 'rxjs';
import { IPokemon } from 'src/app/models/pokemon.model';
import { IPokemonResponse } from './../models/pokemon.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';

const { apiKEY, pokemonApiURL, trainerApiURL } = environment;
@Injectable({
  providedIn: 'root',
})
export class PokedexService {
  constructor(private http: HttpClient) {}

  public pokedex(pokemon?: string): any {
    if (pokemon === undefined) {
      return this.http.get<IPokemonResponse>(pokemonApiURL);
    } else {
      return this.http.get<IPokemonResponse>(pokemonApiURL + '/' + pokemon);
    }
  }

  public nextPage(nextUrl: string): any {
    return this.http.get<IPokemonResponse>(nextUrl);
  }

  public previousPage(previousUrl: string): any {
    return this.http.get<IPokemonResponse>(previousUrl);
  }

  public addPokemonToTrainer(
    userId: number,
    pokemons: IPokemon[]
  ): Observable<IUser> {
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKEY,
    });

    return this.http.patch<IUser>(
      trainerApiURL + '/' + userId,
      {
        pokemon: [...pokemons],
      },
      {
        headers,
      }
    );
  }
}
