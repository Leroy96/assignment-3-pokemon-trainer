import { UserService } from './services/user.service';
import { Component, OnInit } from '@angular/core';
import { IUser } from './models/user.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Pokemon Trainer - Angular';

  get user(): IUser | undefined {
    return this.userService.user;
  }

  constructor(private userService: UserService) {}

  ngOnInit() {}

  Logout() {
    sessionStorage.removeItem('user');
  }
}
